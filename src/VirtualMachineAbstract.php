<?php

namespace SHService;


abstract class VirtualMachineAbstract
{

    /**
     * @param array $params
     * @return bool
     */
    public function satisfy(array $params)
    {
        if ($params['RAM'] <= $this->ram ||
            $params['CPU'] <= $this->cpu ||
            $params['HDD'] <= $this->hdd
        ) {
            return false;
        }

        return true;
    }
}
