<?php

namespace SHService;


class VirtualMachine extends VirtualMachineAbstract
{
    public $cpu;
    public $hdd;
    public $ram;

    public function __toString()
    {
        return 'cpu = ' . $this->getCpu() . ', ram=' . $this->getRam() . ', hdd=' . $this->getHdd();
    }

    /**
     * VirtualMachine constructor.
     * @param int $cpu
     * @param int $ram
     * @param int $hdd
     */
    public function __construct(int $cpu, int $ram, int $hdd)
    {
        $this->cpu = $cpu;
        $this->ram = $ram;
        $this->hdd = $hdd;
    }

    /**
     * @return VirtualMachine
     */
    public static function fromObject(): self
    {
        return new self(1, 16, 10);
    }

    /**
     * @return int
     */
    public function getCpu(): int
    {
        return $this->cpu;
    }

    /**
     * @param int $cpu
     */
    public function setCpu(int $cpu)
    {
        $this->cpu = $cpu;
    }

    /**
     * @return int
     */
    public function getHdd(): int
    {
        return $this->hdd;
    }

    /**
     * @param int $hdd
     */
    public function setHdd(int $hdd)
    {
        $this->hdd = $hdd;
    }

    /**
     * @return int
     */
    public function getRam(): int
    {
        return $this->ram;
    }

    /**
     * @param int $ram
     */
    public function setRam(int $ram)
    {
        $this->ram = $ram;
    }
}
