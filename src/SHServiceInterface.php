<?php

namespace SHService;


interface SHServiceInterface
{
    public function calculate(array $serverParams, array $virtualMachines);

    public function getServerCount();
}
