<?php

namespace SHService;


class SHService implements SHServiceInterface
{
    private $serverCount = 0;

    /**
     * @param array $serverParams
     * @param array $virtualMachines
     * @return int
     */
    public function calculate(array $serverParams, array $virtualMachines): int
    {
        foreach ($virtualMachines as $vm) {
            if($vm->satisfy($serverParams)) {
                $this->serverCount++;
            }
        }
        return $this->getServerCount();
    }

    /**
     * @return int
     */
    public function getServerCount(): int
    {
        return $this->serverCount;
    }
}