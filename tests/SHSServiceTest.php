<?php

use PHPUnit\Framework\TestCase;
use SHService\VirtualMachine;
use SHService\SHService;


final class SHSServiceTest extends TestCase
{
    public function testCanBeCreatedFromValidVM(): void
    {
        $VM = new VirtualMachine(1, 16, 10);

        $this->assertInstanceOf(
            VirtualMachine::class,
            VirtualMachine::fromObject($VM)
        );
    }

    public function testCanBeUsedAsObject(): void
    {
        $VM = new VirtualMachine(1, 16, 10);

        $this->assertEquals(
            new VirtualMachine(1, 16, 10),
            VirtualMachine::fromObject($VM)
        );
    }

    public function testCalculator(): void
    {
        //from example of the task
        $VM1 = new VirtualMachine(1, 16, 10);
        $VM2 = new VirtualMachine(1, 16, 10);
        $VM3 = new VirtualMachine(2, 32, 100);

        $result = 2;

        $VMs = [
            $VM1,
            $VM2,
            $VM3
        ];

        $service = new SHService();

        $this->assertEquals(
            $service->calculate([
                'RAM' => 32,
                'CPU' => 2,
                'HDD' => 100
            ],
                $VMs
            ),
            $result
        );

        echo "\r\n";
        echo "result by example for ".$VM1.", ".$VM2.", ".$VM3." is ".$result." virtualMachines";
    }
}
